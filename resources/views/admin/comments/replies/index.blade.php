@extends('layouts.admin')

@section('content')
    <h1>Replies</h1>
    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Author</th>
                <th>Post</th>
                <th>Body</th>
                <th>Active</th>
                <th>Delete</th>
                <th>Created at</th>
            </tr>
        </thead>
        <tbody>
            @if (count($replies)>0)
            @foreach ($replies as $reply)
            <tr>
                <td scope="row">{{$reply->id}}</td>
                <td>{{$reply->author}}</td>
                <td><a href="{{route('admin.comments.index',$reply->comment->id)}}">{{$reply->comment->id}}</a></td>
                <td>{{str_limit($reply->body,50)}}</td>
                <td>
                    @if($reply->is_active==1)
                    {!!Form::open(['method'=>'PATCH','action'=>['CommentRepliesController@update',$reply->id]])!!}
                    <input type="hidden" name="is_active" value="0">
                    <div class="form-group">
                        {!! Form::submit('Un approve', ['class'=>'btn btn-info']) !!}
                    </div>
                    {!!Form::close()!!}
                    @else
                    {!!Form::open(['method'=>'PATCH','action'=>['CommentRepliesController@update',$reply->id]])!!}
                    <input type="hidden" name="is_active" value="1">
                    <div class="form-group">
                        {!! Form::submit('Approve', ['class'=>'btn btn-primary']) !!}
                    </div>
                    {!!Form::close()!!}
                    @endif
                </td>
                <td>
                        {!!Form::open(['method'=>'DELETE','action'=>['CommentRepliesController@destroy',$reply->id]])!!}
                        <div class="form-group">
                            {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
                        </div>
                        {!!Form::close()!!}
                </td>
                <td>{{$reply->created_at->diffForHumans()}}</td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="6">No comments found</td>
            </tr>
            @endif
        </tbody>
    </table>
@endsection