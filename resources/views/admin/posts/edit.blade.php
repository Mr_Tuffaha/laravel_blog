@extends('layouts.admin')

@section('content')
<h1>Edit Post</h1>    
@include('includes.form_error')
{!! Form::model($post,['method'=>'put','action'=>['AdminPostsController@update',$post->id],'files'=>true]) !!}
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('category_id', 'Category:') !!}
    {!! Form::select('category_id',$categories,null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('photo_id', 'Photo:') !!}
    {!! Form::file('photo_id',null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('body', 'Body:') !!}
    {!! Form::textarea('body', null,['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::submit('Edit Post',['class'=>'btn btn-primary']) !!}
</div>
{!! Form::close() !!}

{!! Form::open(['method'=>'delete','action'=>['AdminPostsController@destroy',$post->id]]) !!}

<div class="form-group">
    {!! Form::submit('Delete Post',['class'=>'btn btn-danger']) !!}
</div>
{!! Form::close() !!}
@endsection
