@extends('layouts.admin')

@section('content')
@if(Session::has('flash_user'))
<div class="alert alert-success">
    <strong>Success!</strong> {{session('flash_user')}}
</div>
@endif

<h1>Users</h1>
<table class="table">
    <thead>
        <tr>
            <th>User Id</th>
            <th>Username</th>
            <th>Image</th>
            <th>email</th>
            <th>Role</th>
            <th>Active</th>
            <th>Created at</th>
            <th>Updated at</th>
        </tr>
    </thead>
    <tbody>
        @if(count($users))
        @foreach ($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td><a href="{{route('admin.users.edit',$user->id)}}">{{$user->name}}</a></td>
            <td><img src="{{$user->photo?$user->photo->photo_path:"https://via.placeholder.com/350x150"}}" height="50" alt=""></td>
            <td>{{$user->email}}</td>
            <td>{{$user->role?$user->role->name:"no type"}}</td>
            <td>{{$user->is_active? "Active" : "Not active"}}</td>
            <td>{{$user->created_at->diffForHumans()}}</td>
            <td>{{$user->updated_at->diffForHumans()}}</td>
        </tr>
        @endforeach
        @else
        <tr>
            <td colspan="7" class="table-no-data">
                no users found
            </td>
        </tr>
        
        
        @endif
    </tbody>
</table>
@endsection