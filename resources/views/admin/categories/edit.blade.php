@extends('layouts.admin')

@section('content')
<h1>Edit Category</h1>
@include('includes.form_error')
{!! Form::model($category,['method'=>'patch','action'=>['AdminCategoriesController@update',$category->id]]) !!}
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Edit Category',['class'=>'btn btn-primary']) !!}
    </div>
{!! Form::close() !!}
{!! Form::open(['method'=>'delete','action'=>['AdminCategoriesController@destroy',$category->id]]) !!}
    <div class="form-group">
        {!! Form::submit('Delete Category',['class'=>'btn btn-danger']) !!}
    </div>
{!! Form::close() !!}
@endsection