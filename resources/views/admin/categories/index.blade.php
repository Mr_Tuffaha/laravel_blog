@extends('layouts.admin')

@section('content')
<h1>Categories</h1>
<table class="table">
    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Updated at</th>
            <th>Created at</th>
        </tr>
    </thead>
    <tbody>
        @if (count($categories))
        @foreach ($categories as $category)
        <tr>
            <td scope="row">{{$category->id}}</td>
            <td><a href="{{route('admin.categories.edit',$category->id)}}">{{$category->name}}</a></td>
            <td>{{$category->updated_at->diffForHumans()}}</td>
            <td>{{$category->created_at->diffForHumans()}}</td>
        </tr>
        @endforeach
        @else
            <tr>
                <td colspan="100" class="table-no-data">
                    No categories found
                </td>
            </tr>
        @endif
        
        
    </tbody>
</table>
@endsection