#!/bin/bash
php artisan cache:clear
php artisan route:clear
php artisan config:clear
php artisan key:generate
php artisan migrate:fresh