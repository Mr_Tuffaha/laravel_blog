<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Post;
use App\Photo;
use App\Category;
use App\Http\Requests\PostCreateRequest;
use Illuminate\Support\Facades\Auth;

class AdminPostsController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        //
        $posts = Post::paginate(2);
        return view('admin.posts.index',compact('posts'));
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
        $categories = Category::pluck('name','id');
        return view('admin.posts.create',compact('categories'));
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(PostCreateRequest $request)
    {
        //
        $input =  $request->all();
        $user = Auth::user();
        $input['photo_id']=0;
        if($file = $request->file('photo_id')){
            $name = time().$file->getClientOriginalName();
            $photo = Photo::create(['photo_path'=>$name]);
            $file->move('images',$name);
            $input['photo_id'] = $photo->id;
        }
        if($input['photo_id']==0) unset($input['photo_id']);
        $user->posts()->create($input);
        return redirect(route('admin.posts.index'));
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
        $categories = Category::pluck('name','id');
        $post = Post::findOrFail($id);
        return view('admin.posts.edit',compact('categories','post'));
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        //
        $input =  $request->all();
        $post = Post::findOrFail($id);
        $input['photo_id']=0;
        if($file = $request->file('photo_id')){
            if($post->photo){
                if(file_exists(public_path().$post->photo->photo_path)){
                    unlink(public_path().$post->photo->photo_path);
                }
                $post->photo()->delete();
            }
            $name = time().$file->getClientOriginalName();
            $photo = Photo::create(['photo_path'=>$name]);
            $file->move('images',$name);
            $input['photo_id'] = $photo->id;
        }
        if($input['photo_id']==0) unset($input['photo_id']);
        Auth::user()->posts()->whereId($id)->first()->update($input);
        // $post->update($input);
        return redirect(route('admin.posts.index'));
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        //
        $post = Post::findOrFail($id);
        if($post->photo){
            if(file_exists(public_path().$post->photo->photo_path)){
                unlink(public_path().$post->photo->photo_path);
            }
            $post->photo()->delete();
        }
        $post->delete();
        return redirect(route('admin.posts.index'));
    }

    public function getPost($id){
        $post = Post::findOrFail($id);
        return view('post',compact('post'));
    }
}
