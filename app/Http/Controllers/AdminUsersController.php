<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Photo;
use App\Http\Requests;
use App\Http\Requests\UsersRequest;
use App\Http\Requests\UserEditRequest;
use Illuminate\Support\Facades\Session;
class AdminUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users = User::All();
        return view('admin.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name','id');
        return view('admin.users.create',compact('roles'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersRequest $request)
    {
        
        $input = $request->all();
        $input['photo_id'] = 0;
        if($file = $request->file('photo_id')){
            $name = time().$file->getClientOriginalName();
            $photo = Photo::create(['photo_path'=>$name]);
            $file->move('images',$name);
            $input['photo_id'] = $photo->id;

        }
        $input['password'] = password_hash($input['password'],PASSWORD_DEFAULT);
        User::create($input);
        Session::flash('flash_user','User has been created');
        return redirect('admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::findOrFail($id);
        $roles = Role::pluck('name','id')->all();
        return view('admin.users.edit',compact('roles','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserEditRequest $request, $id)
    {
        //

        $user = User::findOrFail($id);
        $input = $request->all();
        if(rtrim($input['password'])==''){
            unset($input['password']);
        }else{
            $input['password'] = password_hash($input['password'],PASSWORD_DEFAULT);
        }
        // return $input;
        $input['photo_id'] = 0;
        if($file = $request->file('photo_id')){
            if($user->photo){
                if(file_exists(public_path().$user->photo->photo_path)){
                    unlink(public_path().$user->photo->photo_path);
                }
                $user->photo()->delete();
            }
            $name = time().$file->getClientOriginalName();
            $photo = Photo::create(['photo_path'=>$name]);
            $file->move('images',$name);
            $input['photo_id'] = $photo->id;
        }
        if(!$input['photo_id']) unset($input['photo_id']);
        $user->update($input);
        Session::flash('flash_user','User has been updated');
        return redirect('admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::findOrFail($id);
        if($user->photo){
            if(file_exists(public_path().$user->photo->photo_path)){
                unlink(public_path().$user->photo->photo_path);
            }
            $user->photo()->delete();
        }
        $user->delete();
        Session::flash('flash_user','User has been deleted');
        return redirect('/admin/users');
    }
}
